# MoneyTravel backend

This repository contains the source code for database schema, REST API and WebSocket API used by the client applications. Infrastructure is managed through Docker Compose.

Files of note:
* [REST and WebSocket APIs definition](api/api.js) (api/api.js)
* [Database schema](db/2_init_schema.sql) (db/2_init_schema.sql)
* [Infrastructure configuration](docker-compose.yml) (docker-compose.yml)
